import { Component, OnInit } from '@angular/core';
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  employees: Employee[] = [];
  selectedEmployee? :Employee;
  searchText:any;

   constructor(private employeeService: EmployeeService) {
  }

  ngOnInit(): void {
    this.getEmployees();
  }

  onDetails( employee: Employee ):void {
    this.selectedEmployee = employee;
  }

  onDelete( employee: Employee ):void {
    this.employeeService.deleteEmployee(employee);
  }

  createEmployee():void{

  }

  getEmployees():void{
    this.employeeService.getEmployees().subscribe(employees => this.employees = employees);
  }
}
