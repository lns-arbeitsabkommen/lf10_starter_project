import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ListComponent} from "./list/list.component";
import {EmployeeDetailsComponent} from "./employee-details/employee-details.component";
import {CreateEmployeeComponent} from "./create-employee/create-employee.component";

const routes: Routes = [
  { path: '', redirectTo: 'employees', pathMatch: 'full' },
  { path: 'employees', component: ListComponent },
  { path: 'detail/:id', component: EmployeeDetailsComponent },
  { path: 'detail', component: EmployeeDetailsComponent },
  {path: 'create', component: CreateEmployeeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
