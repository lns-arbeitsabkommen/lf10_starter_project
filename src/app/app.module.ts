import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import { ListComponent } from './list/list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import {Ng2SearchPipeModule} from "ng2-search-filter";
import { CreateEmployeeComponent } from './create-employee/create-employee.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    EmployeeDetailsComponent,
    CreateEmployeeComponent
  ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
        Ng2SearchPipeModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
