export class AuthUtilities {
    private _token: string = '';
    private _expiresIn: number | null = null;

    get expiresIn():number{
        return this.expiresIn;
    }

    set expiresIn(expiresIn: number | null){
        this._expiresIn = expiresIn;
    }

    get token():string{
        return this._token;
    }

    set token(token: string){
        this._token = token;
    }
}
