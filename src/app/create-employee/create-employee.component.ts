import {Component, Input, OnInit} from '@angular/core';
import {EmployeeService} from "../employee.service";
import {Router} from "@angular/router";
import {Employee} from "../Employee";

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  @Input('firstName') firstName!: string;
  @Input('lastName') lastName!: string;
  @Input('street') street!: string;
  @Input('postcode') postcode!: string;
  @Input('city') city!: string;
  @Input('phone') phone!: string;

  submitted = false;

  constructor(private employeeService: EmployeeService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onCreate(): void {
    let employee = new Employee();
    employee.firstName = this.firstName;
    employee.lastName = this.lastName;
    employee.street = this.street;
    employee.postcode = this.postcode;
    employee.city = this.city;
    employee.phone = this.phone;

    console.log(employee); //Für Debugzwecke, einmal den erstellten Employee in Console ausgeben

    this.employeeService.createEmployee(employee).subscribe(() => this.router.navigateByUrl(""));
  }
}
