import {Component} from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = "Employee Management";

  sessionExpired(){
    const authUtils = localStorage.getItem('authUtilities');
    if( authUtils !==null && Number(JSON.parse(authUtils)._expiresIn) <= Date.now() ){
      localStorage.removeItem('authUtilities')
    }

  }

  loggedIn(){
    this.sessionExpired();
    return localStorage.getItem('authUtilities') !== null;
  }
}
