import {Token} from '@angular/compiler/src/ml_parser/tokens';
import {AuthUtilities} from '../AuthUtilities';
import {Component, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() user: string = '';
  @Input() password: string = '';

  authUtilities: AuthUtilities;

  constructor() {
    this.authUtilities = new AuthUtilities();
  }

  ngOnInit(): void {
  }


  async onLogin() {
    try {
      const response = await fetch('/auth',{
        method:'POST',
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'grant_type=password&client_id=employee-management-service&username='+this.user+'&password='+this.password
      });
      console.error(response.url);

      const body = await response.json();
      this.setAuthUtilities(body);
      localStorage.setItem('authUtilities', JSON.stringify(this.authUtilities))
    } catch (e) {
      console.error(e);
      this.handleError();
    }
  }

  handleError() {
    (document.getElementById("username") as HTMLInputElement).style.borderColor = "red";
    (document.getElementById("password") as HTMLInputElement).style.borderColor = "red";
  }

  setAuthUtilities(response: any) {
    this.authUtilities.token = response.access_token;
    this.authUtilities.expiresIn = Date.now() + response.expires_in * 1000;
  }

}
