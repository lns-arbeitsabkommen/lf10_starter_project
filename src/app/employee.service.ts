import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {Employee} from './Employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private employeeUrl: string = '/backend'

  private httpOptions: object = {
    headers: new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.getBearer()}`)
  };

  constructor(private http: HttpClient) {
  }

  getEmployees(): Observable<Employee[]> {
    const url = `${this.employeeUrl}`;
    return this.http.get<Employee[]>(url, this.httpOptions)
      .pipe(catchError(this.handleError<Employee[]>(`this.getEmployees`)));
  }

  /**
   * GET employee by id.
   * Will return Code 404 if it can't find employees
   */
  getEmployee(id: number): Observable<Employee> {
    const url = `${this.employeeUrl}/${id}`;

    return this.http.get<Employee>(url, this.httpOptions).pipe(catchError(this.handleError<Employee>(`this.getEmployee id=${id}`)));
  }

  /*
  * PUT
  * Updates employee on server
  */
  updateEmployee(employee: Employee): Observable<any> {
    const url = `${this.employeeUrl}/${employee.id}`;

    return this.http.put(url, employee, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateEmployee'))
    );
  }

  /*
  * POST
  * Creates an employee on server
  * */
  createEmployee(employee: Employee): Observable<Employee> {
    const url = `${this.employeeUrl}`;
    return this.http.post(url, employee, this.httpOptions).pipe(
      catchError(this.handleError<any>('createEmployee'))
    );
  }

  /*
 * DELETE
 * Deletes an employee on server
 * */
  deleteEmployee(employee: Employee): Observable<any> {
    const url = `${this.employeeUrl}/${employee.id}`;
    return this.http.delete(url, this.httpOptions).pipe(
      catchError(this.handleError<any>('deleteEmployee'))
    );
  }

  getBearer(): string {
    //return "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDQ5NjkyMjIsImlhdCI6MTY0NDk1NDgyMiwianRpIjoiYzg0NGU0YTQtNDFkMC00MzYwLWI4NDMtOTQwOGUyNDY2YTZhIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI3OGJlMzNlZi1iNzI3LTQ4NTQtOWRmZC02NWJkYzJjZjEwYjYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.KlwrV3nqtSMhU4aGLsDBn7Wqc9xIioes8kIuB2AkpRg8MEnI5DdM9v47Q99IwEeL6GQUSjZ1g4Y9MNhNRZOjXikzFPfSC9IcxcOvQMVoO1PgrDyC9PRGRX9hDoJXCKKICQ5kgU841v43OHc8wcO5cXnMvxDsniY13jDWNvuKyr86aLgf0q7rwi6uCMGIMnmxhVitcLwnx-fYz9DFYppY_kUJ0pHOx7pMUJ5x7ODQZ8tQLjqa9WAyLtMhA65MkaWSm-TM1Dasb4Xc3nqK7DAto5o3-SzY-I-F8mVz0XokmUBT-EDdAzWUt4J8OYrv-8m6CZU3LUz2MtAXTsHOwAGBkA";

    const authUtils = localStorage.getItem('authUtilities');
    if (authUtils !== null) {
      return String(JSON.parse(authUtils)._token);
    } else {
      return '';
    }
  }

  /*
  * ErrorHandling
  */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      // Keeps app running by returning an empty result.
      return of(result as T);
    };
  }
}
